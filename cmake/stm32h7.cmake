set(TARGET opencm3_api)

add_library(${TARGET} INTERFACE)
add_library(opencm3::api ALIAS ${TARGET})

target_include_directories(${TARGET}
    INTERFACE
    include
    )

target_compile_definitions(${TARGET}
    INTERFACE
    -DSTM32H7
    )

set(TARGET opencm3_startup)

add_library(${TARGET} STATIC)
add_library(opencm3::startup ALIAS ${TARGET})

target_sources(${TARGET}
    PRIVATE
    include/libopencm3/stm32/h7/nvic.h

    lib/cm3/scb.c
    lib/cm3/vector.c
    lib/cm3/cpp_defines.cpp
    )

target_include_directories(${TARGET}
    PRIVATE
    include
    )

target_link_directories(${TARGET}
    INTERFACE
    lib
    )

target_compile_definitions(${TARGET}
    PRIVATE
    -DSTM32H7
    )

target_link_options(${TARGET} 
    INTERFACE
    LINKER:--undefined=__dso_handle
    )

set(TARGET opencm3_impl)

add_library(${TARGET} STATIC)
add_library(opencm3::impl ALIAS ${TARGET})

target_sources(${TARGET}
    PRIVATE
    include/libopencm3/stm32/h7/nvic.h

    lib/cm3/systick.c
    lib/cm3/nvic.c

    lib/stm32/h7/fdcan.c
    lib/stm32/h7/pwr.c
    lib/stm32/h7/rcc.c

    lib/stm32/common/dac_common_all.c
    lib/stm32/common/dac_common_v2.c
    lib/stm32/common/exti_common_all.c
    lib/stm32/common/flash_common_all.c
    lib/stm32/common/flash_common_f.c
    lib/stm32/common/flash_common_f24.c
    lib/stm32/common/fmc_common_f47.c
    lib/stm32/common/gpio_common_all.c
    lib/stm32/common/gpio_common_f0234.c
    lib/stm32/common/rcc_common_all.c
    lib/stm32/common/rng_common_v1.c
    lib/stm32/common/timer_common_all.c
    lib/stm32/common/usart_common_v2.c
    lib/stm32/common/usart_common_fifos.c
    lib/stm32/common/quadspi_common_v1.c
    )

target_include_directories(${TARGET}
    PRIVATE
    include
    )

target_compile_definitions(${TARGET}
    PRIVATE
    -DSTM32H7
    )

set(TARGET opencm3)

add_library(${TARGET} INTERFACE)
add_library(opencm3::opencm3 ALIAS ${TARGET})

target_link_libraries(${TARGET}
    INTERFACE
    opencm3_api
    opencm3_impl
    )

set(IRQ_INPUT include/libopencm3/stm32/h7/irq.json)
add_custom_command(
    OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/include/libopencm3/stm32/h7/nvic.h
    COMMAND ${PYTHON} ${CMAKE_CURRENT_SOURCE_DIR}/scripts/irq2nvic_h ./${IRQ_INPUT}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${IRQ_INPUT}
    )
